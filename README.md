# PROYECTO DE REACT AUX SIS313G1 - I/2024
**Created by:** Univ. Sergio Moises Apaza Caballero

## LINK DEL TEMPLATE EN FIGMA
https://www.figma.com/file/mzl689uPglPDVQIjxfW7hB/React-auxsis313?type=design&node-id=0%3A1&mode=design&t=tAND3qVWmySEOKry-1

## LINK DEL REPOSITORIO PRINCIPAL
https://gitlab.com/Zergio23/aux-sis313g1-i24.git

## LINK NETLIFY
https://portfolio-auxsis313-i24.netlify.app

## AVANCES DEL TEMPLATE
- [X] Header
- [X] Skills Section
- [ ] Experience Section
- [ ] About Me Section
- [ ] Project Section
- [ ] Testimonial Section
- [ ] Contact Section
- [ ] Footer