'use client';

import Image from "next/image";
import styles from "./page.module.css";
import { Box, Button, Card, Typography } from "@mui/material";
import { selection } from "@/helpers/helpers";
import { useRef } from "react";
import Header from "@/components/Header";
import MyButton from "@/components/MyButton";
import Title from "@/components/Title";
import Skill from "@/components/Skill";
import ExperienceBox from "@/components/ExperienceBox";

export default function Home() {
  const myRef:any = useRef(null);

  return (
    <main>
      <Header/>
      <Box className="skill-section">
        <Title title="my skills" color="#000"/>
        <Box className="skill-container">
          <Skill text="git" imgSrc="/images/skills/git.svg"/>
          <Skill text="javascript" imgSrc="/images/skills/javascript.svg"/>
          <Skill text="sass/Scss" imgSrc="/images/skills/sass.svg"/>
          <Skill text="nest.Js" imgSrc="/images/skills/nest.svg"/>
          <Skill text="storybook" imgSrc="/images/skills/storybook.svg"/>
          <Skill text="nest.Js" imgSrc="/images/skills/nest.svg"/>
          <Skill text="git" imgSrc="/images/skills/git.svg"/>
          <Skill text="storybook" imgSrc="/images/skills/storybook.svg"/>
          <Skill text="socket.Io" imgSrc="/images/skills/socket.svg"/>
          <Skill text="sass/Scss" imgSrc="/images/skills/sass.svg"/>
        </Box>
      </Box>
      <Box className="experience-section">
        <Title title="my experience" color="#fff"/>
        <ExperienceBox logoUrl="/images/google-logo.svg" title="Lead Software Engineer at Google" startDate="Nov 2019" description="As a Senior Software Engineer at Google, I played a pivotal role in developing innovative solutions for Google's core search algorithms. Collaborating with a dynamic team of engineers, I contributed to the enhancement of search accuracy and efficiency, optimizing user experiences for millions of users worldwide."/>
        <ExperienceBox logoUrl="/images/youtube-logo.svg" title="Software Engineer at Youtube" startDate="Jan 2017  " endDate="Oct 2019" description="As a Senior Software Engineer at Google, I played a pivotal role in developing innovative solutions for Google's core search algorithms. Collaborating with a dynamic team of engineers, I contributed to the enhancement of search accuracy and efficiency, optimizing user experiences for millions of users worldwide."/>
        <ExperienceBox logoUrl="/images/apple-logo.svg" title="Junior Software Engineer at Apple" startDate="Jan 2016" endDate="Dec 2017" description="As a Senior Software Engineer at Google, I played a pivotal role in developing innovative solutions for Google's core search algorithms. Collaborating with a dynamic team of engineers, I contributed to the enhancement of search accuracy and efficiency, optimizing user experiences for millions of users worldwide."/>
      </Box>
      <Box className="about-section">
        <Box>
          <Image alt="profile photo" src="/images/myPhoto.svg" width={100} height={100}/>
        </Box>
        <Box>
          <Title title="about me" color="#000"/>
          <Typography variant="body1" className="paragraph-main">
            I'm a passionate, self-proclaimed designer who specializes in full stack development (React.js & Node.js). I am very enthusiastic about bringing the technical and visual aspects of digital products to life. User experience, pixel perfect design, and writing clear, readable, highly performant code matters to me.
            <br /><br />
            I began my journey as a web developer in 2015, and since then, I've continued to grow and evolve as a developer, taking on new challenges and learning the latest technologies along the way. Now, in my early thirties, 7 years after starting my web development journey, I'm building cutting-edge web applications using modern technologies such as Next.js, TypeScript, Nestjs, Tailwindcss, Supabase and much more.
            <br /><br />
            When I'm not in full-on developer mode, you can find me hovering around on twitter or on indie hacker, witnessing the journey of early startups or enjoying some free time. You can follow me on Twitter where I share tech-related bites and build in public, or you can follow me on GitHub.
          </Typography>
        </Box>
      </Box>
    </main>
  );
}

{/* <main id="main-container">
<Card id="selection-container">
    <Typography variant="h4" className="selection-bold-text">Welcome to my PORTFOLIO</Typography>
    <Typography variant="body1">
      This project was created with <span className="selection-bold-text">React</span> using <span className="selection-bold-text">Next.js</span>, adding <span className="selection-bold-text">MaterialUI</span>
      <br />
      Please, choose one option
    </Typography>
    <Box>
      <Button variant="contained" id="select-button">Male</Button>
      <Button variant="contained" id="select-button">Female</Button>
    </Box>
</Card>
<div id="page-container">

</div>
</main> */}

// const mainContainer: any = document.getElementById('main-container')
// const selectContainer: any = document.getElementById('selection-container')
// const pageContainer: any = document.getElementById('page-container')
// const selectButton: any = document.getElementById('select-button')

// selectButton.addEventListener('click', () => {
//   console.log('yes')
// })
// const selection2 = () => {
//   // selectContainer.style.backgroundColor = "red";
//   // console.log("getin")
//   mainContainer.removeChild(selectContainer)
//   // myRef.current.style.backgroundColor = "red";
// }