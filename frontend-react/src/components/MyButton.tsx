import { Button } from "@mui/material"
import Image from "next/image";

interface buttonProps {
    icon?: boolean,
    text: string
}

const MyButton = (props: buttonProps) => {
    const {icon, text} = props;
    return(
        props.icon?
        <Button sx={{ display: { xs: 'none', sm: 'flex' } }} variant='contained' className='nav-button'>
          {text}
          <Image alt="download" src="/images/button-download.svg" width={100} height={100}  />
        </Button>
        :
        <Button sx={{ display: { xs: 'none', sm: 'flex' } }} variant='outlined' className='nav-button'>
          {text}
        </Button>
    )
}

export default MyButton