import { Box, Typography } from "@mui/material"
import Image from "next/image"

interface experienceProps {
    logoUrl: string,
    title: string,
    startDate: string,
    endDate?: string,
    description: string
}

const ExperienceBox = (props: experienceProps) => {
    const {logoUrl, title, startDate, endDate, description} = props
    return(
        endDate?
            <Box className="experience-box">
                <Box sx={{...titleExperienceStyle, marginBottom: "20px"}}>
                    <Box sx={titleExperienceStyle}>
                        <Image style={logoStyle} alt="experience-logo" src={logoUrl} width={100} height={100}/>
                        <Typography className="" variant="h5">{title}</Typography>
                    </Box>
                    <Box>
                            <Typography className="paragraph-secondary" variant="body2">{startDate} - {endDate}</Typography>
                    </Box>
                </Box>
                <Box>
                    <Typography className="paragraph-secondary" variant="body2">{description}</Typography>
                </Box>
            </Box>
        :
            <Box className="experience-box">
                <Box sx={{...titleExperienceStyle, marginBottom: "20px"}}>
                    <Box sx={titleExperienceStyle}>
                        <Image style={logoStyle} alt="experience-logo" src={logoUrl} width={100} height={100}/>
                        <Typography variant="h5">{title}</Typography>
                    </Box>
                    <Box>
                            <Typography className="paragraph-secondary" variant="body2">{startDate} - Present</Typography>
                    </Box>
                </Box>
                <Box>
                    <Typography className="paragraph-secondary" variant="body2">{description}</Typography>
                </Box>
            </Box>
    )
}

const titleExperienceStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
}
const logoStyle = {
    width: 32,
    height: 32,
    marginRight: 30
}

export default ExperienceBox