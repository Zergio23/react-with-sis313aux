import { Button } from "@mui/material"
import * as Icons from '@mui/icons-material'

interface SocialButtonProps {
    iconName: keyof typeof Icons;
    [key: string]: any; // Esto permite pasar cualquier otra prop adicional
}

const SocialButton: React.FC<SocialButtonProps> = ({ iconName, ...props }) => {
    const IconComponent = Icons[iconName];

    if (!IconComponent) {
        return null; // or return a default icon
    }

    return (
        <Button className="social-button">
            <IconComponent {...props}/>
        </Button>
    )
}

export default SocialButton