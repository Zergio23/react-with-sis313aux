import { Typography } from "@mui/material";

interface titleProps {
    color: string,
    title: string
}

const Title = (props: titleProps) => {

    const {color, title} = props;

    const getFirstWord = (text: string) => {
        const words = text.trim().split(/\s+/);
        return words.length > 0 ? words[0] : '';
    };
    const getLastWord = (text: string) => {
        const words = text.trim().split(/\s+/);
        return words.length > 0 ? words[words.length - 1] : '';
    };
    
    const firstWord = getFirstWord(title);
    const lastWord = getLastWord(title);

    return(
        <Typography variant="h3" sx={{...titleStyle, color: {color}}}>{firstWord} <span style={secondTitle}>{lastWord}</span></Typography>
    )
}

const titleStyle = {
    fontSize: "3em",
    textTransform: "capitalize",
    textAlign: "center",
    fontFamily: '"Sora", sans-serif'
}
const secondTitle = {
    fontWeight: 800
}

export default Title