import { Box, Typography } from "@mui/material"
import Image from "next/image"

interface skillProps {
    imgSrc: string,
    text: string
}

const Skill = (props: skillProps) => {
    const {imgSrc, text} = props
    return(
        <Box className="skill">
            <Image alt="skill-logo" src={imgSrc} width={100} height={100}></Image>
            <Typography variant="body1">{text}</Typography>
        </Box>
    )
}

export default Skill