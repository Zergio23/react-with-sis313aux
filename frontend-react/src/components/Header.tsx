import { Box, Typography } from "@mui/material"
import Navbar from "./Navbar"
import Image from "next/image"
import SocialGroup from "./SocialGroup"

const Header = () => {
    return(
        <Box>
            <Navbar/>
            <Box className="banner">
                <Box>
                    <Typography variant="h3" className="title-banner">
                        Hello I’am <span className="title-banner-bold">Evren Shah. Frontend <span className="title-banner-double">Developer</span></span> Based In <span className="title-banner-bold">India.</span>
                    </Typography>
                    <Typography variant="body1" className="paragraph-main">I'm Evren Shah Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to specimen book.</Typography>
                </Box>
                <Box className="banner-social">
                        <SocialGroup/>
                    </Box>
                <Box>
                    <Image alt="banner-img" src="/images/banner-img.svg" width={100} height={100}></Image>
                </Box>
            </Box>
        </Box>
    )
}

export default Header