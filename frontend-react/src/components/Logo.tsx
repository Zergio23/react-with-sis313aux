import { WidthFull } from "@mui/icons-material"
import { Box, Typography, colors } from "@mui/material"
import Image from "next/image"

const Logo = () => {
    return(
        <Box sx={boxStyle}>
            <Image alt="logo" src="/images/logo.svg" width={100} height={100} style={{...logoStyle}}/>
            <Typography variant="h6" className="logo-text">Principal</Typography>
        </Box>
    )
}

const logoStyle = {
    width: "23.15px",
    height: "34.73px"
}
const boxStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "139.91px",
    cursor: "pointer"
}

export default Logo