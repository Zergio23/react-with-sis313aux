import { Box, IconButton } from "@mui/material"
import SocialButton from "./SocialButton"
// import { FacebookRounded, Reddit, X } from "@mui/icons-material"

const SocialGroup = () => {
    return(
        <Box className="social-group">
            <SocialButton iconName="FacebookRounded" />
            <SocialButton iconName="Reddit" />
            <SocialButton iconName="X" />
            <SocialButton iconName="Instagram" />
        </Box>
    )
}

export default SocialGroup