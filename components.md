# COMPONENTES
## GENERAL
**Button:**  
![button](/components-img/button.png)  
**Changes:**  
- Icon at side

**Keeps:**  
- Text
- Button-style

**Social Button:**  
![social-button](/components-img/social-button.png)   
**Changes:**  
- Logo
- Text

**Keeps:**  
- Border-style
- Border-radius
- Color
- Hover

**Social Section:**  
![social-section](/components-img/social-section.png)  
**Changes:**  
- *Nothing*

**Keeps:**  
- Social button component

**Logo:**  
![logo](/components-img/logo.png)  
**Changes:**  
- Color

**Keeps:**  
- Icon
- Text

**Title:**  
![title](/components-img/title.png)  
**Changes:**  
- Color

**Keeps:**  
- Text

## HEADER SECTION  
**Navigation bar:**  
![navbar](/components-img/navbar.png)  
**Changes:**  
- *Nothing*

**Keeps:**  
- Lists
- Button
- Logo

## SKILLS SECTION  
**Skill:**  
![skill](/components-img/skill.png)  
**Changes:**  
- Logo
- Text

**Keeps:**  
- Style

## EXPERIENCE SECTION  
**Experience:**  
![experience](/components-img/experience.png)  
**Changes:**  
- Logo
- Title
- Paragraph
- Date

**Keeps:**  
- Style

## PROJECTS SECTION  
**Project:**  
![project](/components-img/project.png)  
**Changes:**  
- Image
- Title
- Paragraph
- Component direction

**Keeps:**  
- Style
- Icon to external url

## TESTIMONIAL SECTION  
**Testimonial:**  
![testimonial](/components-img/testimonial.png)  
**Changes:**  
- Avatar photo
- Description
- Name
- Profession

**Keeps:**  
- Circular image at side of avatar
- Horizontal line (hr)